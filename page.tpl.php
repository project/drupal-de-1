<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>

</head>
  <body class="front not-logged-in sidebars">

    <div id="page" class="clear-block">

      <div id="header">
      <?php if ($logo) : ?>
        <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print($logo) ?>" alt="<?php print t('Home') ?>" border="0" /></a>
      <?php endif; ?>
      </div> <!-- /#header -->

      <div id="main_menu">
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
        <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        <?php endif; ?>
      </div> <!-- /#main_menu -->

      <div id="wrapper" class="clear-block">

        <div id="subwrapper">

          <div id="container">

            <div id="content">

          <?php if ($breadcrumb): print $breadcrumb; endif; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>

          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($tabs): print $tabs .'</div>'; endif; ?>

          <?php if (isset($tabs2)): print $tabs2; endif; ?>

    <?php if ($title != ""): ?>
    <h1 class="title"><?php print $title ?></h1>
    <?php endif; ?>
    <?php if ($help != ""): ?>
    <div id="help"><?php print $help ?></div>
    <?php endif; ?>
    <?php if ($messages != ""): ?>
    <?php print $messages ?> 
    <?php endif; ?>
    <!-- main content -->

    <?php print $content; ?> 

    <!-- /main content -->

            </div>

          </div> <!-- /#container -->

          <div id="left_sidebar">


<?php print $sidebar_left ?>

 

          </div> <!-- /#left_sidebar -->

        </div><!-- /#subwrapper -->

        <div id="right_sidebar">

<?php print $sidebar_right ?>

        </div> <!-- /#right_sidebar -->

      </div> <!-- /#wrapper -->

      <div id="secondary_menu">

      </div> <!-- /#secondary_menu -->

      <div id="footer">

<?php print $footer_message ?>
<br />
<?php print $closure ?>
<? /*
Please do not remove these links!
Bitte entfernen Sie die nachfolgenden Links nicht! 
 */ ?>
<a href="http://www.drupal.de">Drupal</a> <a href="http://www.wohnung.net">-</a>
      </div> <!-- /#footer -->
  
    </div> <!-- /#page -->
  </body>

</html>
